GitLab-Asteroids
===============
Just a fork of a Asteroids clone, modified to have a GitLab spaceship. :)

https://jyavorska.gitlab.io/glasteroids/


Forked from Pure Javascript Asteroids, based on http://dougmcinnes.com/2010/05/12/html-5-asteroids/ 
Original Source https://github.com/dmcinnes/HTML5-Asteroids
